## pc2019-public-code


#### PetriNet DSL
Extended PetriNet DSL by implicitly converting Tuples in MSet.

 ```
 P1 ~~> P2,
 (P2, P3) ~~> P3
 ```
#### PetriNet with prioritized transitions 
Extend PetriNet with prioritized transition. Transitions now can include a priority with the following syntax:

```
val p = 10 //priority
P1 ~p~> P2
```
If not specified the default priority is 0. Higher values have higher priority.

#### Readers-Writers example
Add [ReadersWriters](src/main/scala/pc/examples/PNReadersWriters.scala) example which takes advantage of the new syntax in order to prioritize the readers.

#### Authors
Edoardo Barbieri, Giacomo Tontini