package pc.utils

object RichIterable {

  implicit class RichIterable[A](iterable:Iterable[A]){
    def maxByOption[B](f: A => B)(implicit cmp: Ordering[B]):Option[A] =
      if(iterable.isEmpty) None else Some(iterable.maxBy(f))
  }
}
