package pc.modelling

import pc.utils.MSet
import pc.utils.RichIterable._

object PetriNet {

  type Priority = Int

  // pre-conditions, effects, inhibition
  type PetriNet[P] = Set[(MSet[P], MSet[P], MSet[P], Priority)]

  // factory of a Petri Net
  def apply[P](transitions: (MSet[P], MSet[P], MSet[P], Priority)*): PetriNet[P] =
    transitions.toSet

  def toPartialFunction[P](pn: PetriNet[P]): PartialFunction[MSet[P], Set[MSet[P]]] = {
    case m =>
      val steps = for ((cond, eff, inh, priority) <- pn
                       if m disjoined inh;
                       out <- m extract cond) yield (out union eff, priority)

      steps.maxByOption { case (_, priority) => priority } match {
        case None => Set()
        case Some((_, priority)) => steps.collect { case (mset, `priority`) => mset }
      }
  }

  // factory of a System
  def toSystem[P](pn: PetriNet[P]): System[MSet[P]] =
    System.ofFunction(toPartialFunction(pn))

  // Syntactic sugar to write transitions as:  MSet(s,b,C) ~~> MSet(d,e) or MSet(s,b,C) ~priority~> MSet(d,e)
  implicit class LeftTransitionRelation[P](private val self: MSet[P]) {
    def ~~>(y: MSet[P]): (MSet[P], MSet[P], MSet[P], Priority) = self ~0~> y

    def ~(priority: Int) = new {
      def ~>(y: MSet[P]): (MSet[P], MSet[P], MSet[P], Priority) = (self, y, MSet[P](), priority)
    }

    def ^^^(z: MSet[P]): (MSet[P], MSet[P], MSet[P], Priority) = (self, MSet[P](), z, 0)
  }

  // Syntactic sugar to write transitions as:  MSet(s,b,C) ~~> MSet(d,e) ^^^ MSet(f)
  implicit final class RightTransitionRelation[P](private val self: (MSet[P], MSet[P], MSet[P], Priority)) {
    def ^^^(z: MSet[P]): (MSet[P], MSet[P], MSet[P], Priority) = (self._1, self._2, z, self._4)
  }

  // Syntactic sugar to write transitions as:  (s,b,C) ~~> (d,e)
  implicit def elementToMSet[P](t: P): MSet[P] = MSet(t)
  implicit def tupleToMSet[P](t: (P, P)): MSet[P] = MSet(t._1, t._2)
  implicit def tupleToMSet[P](t: (P, P, P)): MSet[P] = MSet(t._1, t._2, t._3)
  implicit def tupleToMSet[P](t: (P, P, P, P)): MSet[P] = MSet(t._1, t._2, t._3, t._4)
  implicit class elementToLeftTransitionRelation[P](t: P) extends LeftTransitionRelation(elementToMSet(t))
  implicit class tuple2ToLeftTransitionRelation[P](t: (P, P)) extends LeftTransitionRelation(tupleToMSet(t))
  implicit class tuple3ToLeftTransitionRelation[P](t: (P, P, P)) extends LeftTransitionRelation(tupleToMSet(t))
  implicit class tuple4ToLeftTransitionRelation[P](t: (P, P, P, P)) extends LeftTransitionRelation(tupleToMSet(t))
}