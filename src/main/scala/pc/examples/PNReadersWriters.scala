package pc.examples

import pc.modelling.PetriNet
import pc.modelling.PetriNet._


object PNReadersWriters extends App {

  object place extends Enumeration {
    val P1, P2, P3, P4, P5, P6, P7 = Value
  }

  type Place = place.Value

  import place._


  def readersWritersSystem() = toSystem(PetriNet[Place](
    P1 ~~> P2,

    P2 ~~> P3,                  //want to read
    (P3, P5) ~1~> (P6, P5),     //start reading (higher priority)
    P6 ~~> P1,                  //finish reading

    P2 ~~> P4,                  //want to write
    (P4, P5) ~0~> P7 ^^^ P6,    //start writing (lower priority)
    P7 ~~> (P1, P5)             //finish writing
  ))

  println(readersWritersSystem().paths((P1, P1, P5), 6).toList.mkString("\n"))
}
